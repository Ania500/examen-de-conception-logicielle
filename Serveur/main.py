import os
from random import randint

import requests
import uvicorn
from dotenv import load_dotenv
from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def root():
    response1 = requests.get(os.getenv("audio_db_base_url") + "search.php?s=")
    response2 = requests.get(os.getenv("audio_db_base_url") + "album.php?i=")
    response3 = requests.get(os.getenv("audio_db_base_url") + "track.php?m=")
    response4 = requests.get(os.getenv("lyrics_url") + "rick astley" + "/" + "Never gonna give you up")
    return {"AudioDB_id_artist status": response1.status_code, "AudioDB_album status": response2.status_code,
            "AudioDB_track status": response3.status_code, "LyricsOvh status": response4.status_code}


#########################################################################################

@app.get("/artist/{artist_name}")
def get_artist_id(artist_name):
    """Permet de récupérer l'id d'un artiste passé en paramètre
    """
    request_name = requests.get(os.getenv("audio_db_base_url") + "search.php?s=" + artist_name)

    response = request_name.json()
    if response["artists"] is not None:
        id_artist = response["artists"][0]["idArtist"]
    else:
        id_artist = "Artist not found"
    return {"id": id_artist, "status": request_name.status_code}


@app.get("/album/{id_artist}")
def get_id_album(id_artist):
    """Récupère l'id d'un album au hasard à partir de l'id de l'artiste
    """
    request_album = requests.get(os.getenv("audio_db_base_url") + "album.php?i=" + id_artist)
    response_album = request_album.json()
    list_album = response_album["album"]
    length_album = len(list_album)
    id_album = list_album[randint(0, length_album - 1)]["idAlbum"]
    return {"id": id_album}


@app.get("/track/{id_album}")
def get_name_track(id_album):
    """Choisit aléatoirement une chanson de l'album et le lien youtube """
    request_music = requests.get(os.getenv("audio_db_base_url") + "track.php?m=" + id_album)
    response_music = request_music.json()
    list_music = response_music["track"]
    length_music = len(list_music)

    music = list_music[randint(0, length_music - 1)]
    name_track = music["strTrack"]
    url_track = music["strMusicVid"]
    return {"title": name_track, "youtube_link": url_track}


@app.get("/lyrics/{artist_name}/{name_track}")
def get_lyrics(artist_name, name_track):
    """Affiche les lyrics de la chanson de l'artiste passée en paramètre"""
    request_lyrics = requests.get(os.getenv("lyrics_url") + artist_name + "/" + name_track)
    if request_lyrics.status_code == 200:
        response_lyrics = request_lyrics.json()
    else:
        response_lyrics = {"lyrics": "lyrics not found"}
    return response_lyrics


@app.get("/random/{artist_name}")
def get_music_by_artist_name(artist_name: str):
    id_artist = requests.get(os.getenv("app_url") + "/artist/" + artist_name).json()["id"]

    id_album = requests.get(os.getenv("app_url") + "/album/" + id_artist).json()["id"]

    track = requests.get(os.getenv("app_url") + "/track/" + id_album).json()
    name_track = track["title"]
    url_track = track["youtube_link"]

    lyrics = requests.get(os.getenv("app_url") + "/lyrics/" + artist_name + "/" + name_track).json()["lyrics"]

    return {"artist": artist_name, "title": name_track, "suggested_youtube_url": url_track, "lyrics": lyrics}


if __name__ == "__main__":
    load_dotenv()
    uvicorn.run(app, host="127.0.0.1", port=8000, log_level="info")

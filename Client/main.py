import json
import os
import random
import sys

import requests
from dotenv import load_dotenv


def get_artist(file_name):
    """
    Retourne la liste des noms des artistes contenus dans le file_name
    """
    with open(file_name) as my_file:
        data = json.load(my_file)
        list_artist = []
        for element in data:
            list_artist.append(element["artiste"])
        return list_artist


def playlist_played(my_list_artist):
    """Renvoie un dictionnaire contenant les lyrics d'une playlist de 20 chansons
    """
    playlist = dict()
    for i in range(20):
        artist = random.choice(my_list_artist)
        playlist[i] = requests.get(os.getenv("app_url") + "/random/" + artist).json()

    return playlist


def display_playlist(playlist):
    """Affiche la playlist
    """
    for key, value in playlist.items():
        number = key+1
        artist = value["artist"]
        title = value["title"]
        lyrics = value["lyrics"]
        youtube_link = value["suggested_youtube_url"]
        print("Chanson numéro {} de {}: {}".format(number, artist, title))
        print("Lien Youtube:{}".format(youtube_link))
        print("\n")
        print(lyrics)
        print("----------------------------------------------------\n")


if __name__ == "__main__":
    load_dotenv()
    if len(sys.argv) == 2:
        file_name = sys.argv[1]
    else:
        file_name = "rudy.json"
    display_playlist(playlist_played(get_artist(file_name)))

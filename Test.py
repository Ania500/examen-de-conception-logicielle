import os
import unittest

import pytest
import requests
from dotenv import load_dotenv


class Test(unittest.TestCase):
    @pytest.fixture(scope='session', autouse=True)
    def load_env(self):
        load_dotenv()

    def test_get_artist_id(self):
        self.assertNotEqual(requests.get(os.getenv("app_url") + "/artist/" + "rick Astley").json()["id"], "112883")

    def test_get_artist_id2(self):
        self.assertEqual(requests.get(os.getenv("app_url") + "/artist/" + "Ania").json()["id"], "Artist not found")


if __name__ == "__main__":
    unittest.main()

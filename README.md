# Examen de conception de logiciel



## Introduction
L'objectif est de développer une API qui se servira de deux autres API
musicales pour récupérer des informations concernant des 
artistes et leurs chansons. L'application a une partie
serveur et une partie client.
L'application retourne une playlist de 20 chansons d'artistes préférés
de l'utilisateur. Le client doit fournir un fichier json
contenant les noms de ses artistes préférés.

## Fonctionnalités
- Le webservice créé pourra requêter sur les API musicales
pour récupérer l'id d'un artiste, l'id de son album, 
ses chansons, leurs liens youtube et leurs lyrics.
- Le client pourra requêter sur le webservice créé.

## Architecture
```mermaid
graph TD
    Client-->Serveur;
    Serveur -->API
```

## Installation
Pour installer l'application, il faut d'abord commencer
par cloner le fichier git en exécutant dans le
terminal la commande suivante:

```git clone https://gitlab.com/Ania500/examen-de-conception-logicielle.git```

## Installation des dépendances
Il faut commencer par installer les dépendances.
Les différents modules utilisés par l'application sont contenus 
dans les fichiers requirements.txt des dossiers Serveur 
et Client.
Pour installer les modules contenus dans ces dossiers:
### Partie Serveur
- Lancer le serveur: `cd examen-de-conception-logicielle/Serveur` 
- Puis lancer la commande suivante dans le terminal:
```pip install -r requirements.txt```

### Partie Client
- Lancer le client: `cd examen-de-conception-logicielle/Client` 
- Puis lancer la commande suivante dans le terminal:
```pip install -r requirements.txt```

## Lancement de l'application
### Quick start du serveur
Après avoir cloné le dossier git et installé les dépendances:
- Pour ouvrir le dossier Serveur: `cd examen-de-conception-logicielle/Serveur` 
- Lancer la commande dans le terminal :
```python main.py```.


### Quick start du Client
Après avoir installé les dépendances,
- Lancer l'application serveur en suivant le quickstart du serveur.
- Ouvrir un autre terminal
- Ouvrir le dossier Client avec la commande: `cd examen-de-conception-logicielle/Client`
- Puis lancer la commande: ```python main.py <file_name>```

où ``<file_name>``: est un paramètre optionnel qui correspond au chemin du fichier qui contient les noms des
artistes préférés. Le fichier utilisé par défaut est rudy.json.

### Test
Il est possible d'exécuter les tests unitaires 
se trouvant dans le fichier Test.py avec la commande:

```python -m pytest Test.py```

